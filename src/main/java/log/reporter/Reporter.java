package log.reporter;

import java.util.Map;

public interface Reporter {
    void reportNoOfUniqueIP(Long noOfUniqueIP);

    void reportMostVisitedUrls(Map<String, Long> mostVisitedUrls);

    void reportMostActiveIPs(Map<String, Long> mostActiveIPs);
}

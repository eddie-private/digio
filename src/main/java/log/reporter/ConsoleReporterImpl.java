package log.reporter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import log.io.UserIO;

@Component
public class ConsoleReporterImpl implements Reporter {

    @Autowired
    private UserIO userIO;

    @Override
    public void reportNoOfUniqueIP(Long noOfUniqueIP) {
        userIO.displayMessage("* The number of unique IP addresses: " + noOfUniqueIP);
    }

    @Override
    public void reportMostVisitedUrls(Map<String, Long> mostVisitedUrls) {
        userIO.displayMessage("* The top 3 most visited URLs: ");
        mostVisitedUrls.entrySet().forEach(entry -> userIO.displayMessage(entry.getKey() + ". No. of Visits: " + entry.getValue() ));
    }

    @Override
    public void reportMostActiveIPs(Map<String, Long> mostActiveIPs) {
        userIO.displayMessage("* The top 3 most active IP addresses: ");
        mostActiveIPs.entrySet().forEach(entry -> userIO.displayMessage(entry.getKey() + ". No. of Occurrences: " + entry.getValue() ));
    }
}

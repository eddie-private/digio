package log.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import log.entity.LogEvent;
import log.parser.LogParser;

@Repository
public class LogFileIOImpl implements LogFileIO {

    @Autowired
    private FileIO fileIO;

    @Autowired
    private LogParser logParser;

    @Override
    public List<LogEvent> read(String filePath) throws IOException {

        Scanner scanner = new Scanner(fileIO.read(filePath));

        List<LogEvent> logEvents = new ArrayList<>();

        while (scanner.hasNextLine()) {
            logEvents.add(logParser.parse(scanner.nextLine()));
        }

        return logEvents;
    }
}

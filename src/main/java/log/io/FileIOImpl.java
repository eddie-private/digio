package log.io;

import java.io.File;
import java.io.FileNotFoundException;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository
public class FileIOImpl implements FileIO {

    @Override
    public File read(String filePath) throws FileNotFoundException {
        Assert.notNull(filePath, "File path must not be null");

        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException("File " + filePath + " not found");
        }

        return file;
    }
}

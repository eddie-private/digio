package log.io;

import java.io.File;
import java.io.FileNotFoundException;

public interface FileIO {
    File read(String filePath) throws FileNotFoundException;
}

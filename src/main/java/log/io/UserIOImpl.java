package log.io;

import org.springframework.stereotype.Component;

@Component
public class UserIOImpl implements UserIO {
    @Override
    public void displayMessage(String message) {
        System.out.println(message);
    }
}

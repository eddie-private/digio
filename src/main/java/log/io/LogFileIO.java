package log.io;

import java.io.IOException;
import java.util.List;

import log.entity.LogEvent;

public interface LogFileIO {

    List<LogEvent> read(String filePath) throws IOException;

}

package log.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import log.entity.LogEvent;

public interface LogAnalysisService {

    long findNoOfUniqueIP(Collection<LogEvent> logEvents);

    Map<String, Long> findMostVistedURLs(Collection<LogEvent> logEvents, int topNumber);

    Map<String, Long> findMostActiveIPs(Collection<LogEvent> logEvents, int topNumber);
}

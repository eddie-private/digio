package log.service;

import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Service;

import log.entity.LogEvent;
import utils.CollectionUtils;


@Service
public class LogAnalysisServiceImpl implements LogAnalysisService {

    @Override
    public long findNoOfUniqueIP(Collection<LogEvent> logEvents) {
        return CollectionUtils.findNoOfUniqueElements(logEvents, LogEvent::getIpAddress);
    }

    @Override
    public Map<String, Long> findMostVistedURLs(Collection<LogEvent> logEvents, int topNumber) {
        return CollectionUtils.findTopXElements(logEvents, topNumber, LogEvent::getPath);
    }

    @Override
    public Map<String, Long> findMostActiveIPs(Collection<LogEvent> logEvents, int topNumber) {
        return CollectionUtils.findTopXElements(logEvents, topNumber, LogEvent::getIpAddress);
    }
}

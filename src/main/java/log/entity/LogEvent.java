package log.entity;

import java.util.Objects;

public class LogEvent {

    private String ipAddress;
    private String path;
    private String status;

    private String logStatement;

    public LogEvent(String logStatement) {
        this.logStatement = logStatement;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogStatement() {
        return logStatement;
    }

    public void setLogStatement(String logStatement) {
        this.logStatement = logStatement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogEvent logEvent = (LogEvent) o;
        return getLogStatement().equals(logEvent.getLogStatement());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogStatement());
    }
}

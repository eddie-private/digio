package log;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import log.entity.LogEvent;
import log.io.LogFileIO;
import log.io.UserIO;
import log.reporter.Reporter;
import log.service.LogAnalysisService;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final String DEFAULT_FILE_PATH = "src/test/resources/programming-task-example-data.log";

    private static final int NO_OF_MOST_VISITED_URLS = 3;
    private static final int NO_OF_MOST_ACTIVE_IPS = 3;

    @Autowired
    private LogAnalysisService logAnalysisService;

    @Autowired
    private UserIO userIO;

    @Autowired
    private LogFileIO logFileIO;

    @Autowired
    private Reporter reporter;

    @Override
    public void run(String... args) {

        try {
            List<LogEvent> logEvents = logFileIO.read(DEFAULT_FILE_PATH);

            reporter.reportNoOfUniqueIP(logAnalysisService.findNoOfUniqueIP(logEvents));
            reporter.reportMostVisitedUrls(logAnalysisService.findMostVistedURLs(logEvents, NO_OF_MOST_VISITED_URLS));
            reporter.reportMostActiveIPs(logAnalysisService.findMostActiveIPs(logEvents, NO_OF_MOST_ACTIVE_IPS));

        } catch (IOException e) {
            userIO.displayMessage(e.getMessage());
        }
    }

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.setHeadless(true);
        application.setLogStartupInfo(false);
        application.run(args);
    }
}

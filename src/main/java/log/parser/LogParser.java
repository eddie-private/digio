package log.parser;

import log.entity.LogEvent;

public interface LogParser {

    LogEvent parse(String log);

}

package log.parser;

import java.util.Scanner;
import java.util.StringTokenizer;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import log.entity.LogEvent;

@Component
public class LogParserImpl implements LogParser {

    private static final String IP_DELIMITER = " -";
    private static final String REQUEST_DELIMITER = "\"";
    private static final String SPACE_DELIMITER = " ";
    private static final int NO_OF_PARTS_IN_REQUEST = 3;
    private static final int NO_OF_PARTS_IN_STATUS_STRING = 2;

    @Override
    public LogEvent parse(String log) {
        LogEvent logEvent = new LogEvent(log);

        logEvent.setIpAddress(parseIPAddress(log));
        logEvent.setPath(parseURL(log));

        return logEvent;
    }

    private String parseIPAddress(String log) {
        Assert.notNull(log, "Log must not be null");
        return log.contains(IP_DELIMITER) ? new Scanner(log).useDelimiter(IP_DELIMITER).next() : "";
    }

    private String parseURL(String log) {
        Assert.notNull(log, "Log must not be null");

        Scanner scanner = new Scanner(log).useDelimiter(REQUEST_DELIMITER);
        scanner.next();

        String request = scanner.next();

        StringTokenizer stringTokenizer = new StringTokenizer(request, SPACE_DELIMITER);

        if (stringTokenizer.countTokens() == NO_OF_PARTS_IN_REQUEST) {
            stringTokenizer.nextToken();
            return stringTokenizer.nextToken();
        }

        return "";
    }
}

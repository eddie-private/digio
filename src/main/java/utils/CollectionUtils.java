package utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionUtils {

    public static <R, T> Map<R, Long> findTopXElements(Collection<T> targetElements, int topNumber, Function<T, R> classifier) {
        if (targetElements.isEmpty()) {
            return new LinkedHashMap<>();
        }

        Map<R, Long> groupedElements = targetElements.stream().collect(Collectors.groupingBy(classifier, Collectors.counting()));

        LinkedHashMap<R, Long> sortedElements = sortElementsByValue(groupedElements);

        Long topXSize = NumberUtils.findTopXValue(sortedElements.values(), topNumber);

        return trimResults(sortedElements, topXSize);
    }

    public static <T, R> long findNoOfUniqueElements(Collection<T> elements, Function<T,R> mapper) {
        return elements.stream().map(mapper).distinct().count();
    }

    private static <R> LinkedHashMap<R, Long> sortElementsByValue(Map<R, Long> elements) {
        LinkedHashMap<R, Long> sortedElements = new LinkedHashMap<>();
        elements.entrySet().stream().sorted(
                Map.Entry.<R, Long>comparingByValue().reversed()
        ).forEachOrdered(
                entry -> sortedElements.put(entry.getKey(), entry.getValue())
        );

        return sortedElements;
    }

    private static <R> LinkedHashMap<R, Long> trimResults(LinkedHashMap<R, Long> elements, Long topXSize) {
        LinkedHashMap<R, Long> trimmedElements = new LinkedHashMap<>();

        elements.entrySet().stream().takeWhile(entry -> entry.getValue() >= topXSize).forEachOrdered(
                entry -> trimmedElements.put(entry.getKey(), entry.getValue())
        );

        return trimmedElements;
    }
}

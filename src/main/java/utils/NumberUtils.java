package utils;

import java.util.Collection;

import org.springframework.util.Assert;

public class NumberUtils {

    public static long findTopXValue(Collection<Long> values, int topNumber) {
        Assert.notNull(values, "Values must not be null");
        Assert.notEmpty(values, "Values must not be empty");

        int counter = 1;
        Long smallestNumber = values.stream().findFirst().get();

        for (Long value : values) {
            if (value < smallestNumber) {
                smallestNumber = value;

                if (topNumber == (++ counter)) {
                    return smallestNumber;
                }
            }
        }

        return smallestNumber;
    }

}

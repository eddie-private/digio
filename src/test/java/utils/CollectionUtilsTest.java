package utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import log.entity.LogEvent;

public class CollectionUtilsTest {

    private List<LogEvent> logEvents = new ArrayList<>();

    private LogEvent logEvent1 = new LogEvent("1");
    private LogEvent logEvent2 = new LogEvent("2");
    private LogEvent logEvent3 = new LogEvent("3");
    private LogEvent logEvent4 = new LogEvent("4");
    private LogEvent logEvent5 = new LogEvent("5");
    private LogEvent logEvent6 = new LogEvent("6");
    private LogEvent logEvent7 = new LogEvent("7");
    private LogEvent logEvent8 = new LogEvent("8");
    private LogEvent logEvent9 = new LogEvent("9");
    private LogEvent logEvent10 = new LogEvent("10");
    private LogEvent logEvent11 = new LogEvent("11");
    private LogEvent logEvent12 = new LogEvent("12");
    private LogEvent logEvent13 = new LogEvent("13");
    private LogEvent logEvent14 = new LogEvent("14");

    private LinkedHashMap<String, Long> mostVisitedUrls = new LinkedHashMap<>();
    private LinkedHashMap<String, Long> mostActiveIPs = new LinkedHashMap<>();

    @BeforeEach
    public void setup() throws IOException {


        logEvent1.setIpAddress("ip1");
        logEvent2.setIpAddress("ip2");
        logEvent3.setIpAddress("ip3");
        logEvent4.setIpAddress("ip5");
        logEvent5.setIpAddress("ip1");
        logEvent6.setIpAddress("ip6");
        logEvent7.setIpAddress("ip1");
        logEvent8.setIpAddress("ip1");
        logEvent9.setIpAddress("ip2");
        logEvent10.setIpAddress("ip3");
        logEvent11.setIpAddress("ip6");
        logEvent12.setIpAddress("ip6");
        logEvent13.setIpAddress("ip6");
        logEvent14.setIpAddress("ip6");


        logEvent1.setPath("path5");
        logEvent2.setPath("path2");
        logEvent3.setPath("path3");
        logEvent4.setPath("path2");
        logEvent5.setPath("path5");
        logEvent6.setPath("path4");
        logEvent7.setPath("path5");
        logEvent8.setPath("path5");
        logEvent9.setPath("path2");
        logEvent10.setPath("path3");
        logEvent11.setPath("path4");
        logEvent12.setPath("path1");
        logEvent13.setPath("path6");
        logEvent14.setPath("path7");

        logEvents.add(logEvent1);
        logEvents.add(logEvent2);
        logEvents.add(logEvent3);
        logEvents.add(logEvent4);
        logEvents.add(logEvent5);
        logEvents.add(logEvent6);
        logEvents.add(logEvent7);
        logEvents.add(logEvent8);
        logEvents.add(logEvent9);
        logEvents.add(logEvent10);
        logEvents.add(logEvent11);
        logEvents.add(logEvent12);
        logEvents.add(logEvent13);
        logEvents.add(logEvent14);

        mostVisitedUrls.put("path5", 4l);
        mostVisitedUrls.put("path2", 3l);
        mostVisitedUrls.put("path3", 2l);
        mostVisitedUrls.put("path4", 2l);

        mostActiveIPs.put("ip6", 5l);
        mostActiveIPs.put("ip1", 4l);
        mostActiveIPs.put("ip2", 2l);
        mostActiveIPs.put("ip3", 2l);
    }

    @Test
    public void findTop3Elements() {
        assertEquals(mostVisitedUrls, CollectionUtils.findTopXElements(logEvents, 3, LogEvent::getPath));
    }

    @Test
    public void findTop3IPs() {
        assertEquals(mostActiveIPs, CollectionUtils.findTopXElements(logEvents, 3, LogEvent::getIpAddress));
    }

    @Test
    public void findNoOfUniqueIP() {
        assertEquals(5, CollectionUtils.findNoOfUniqueElements(logEvents, LogEvent::getIpAddress));
    }
}

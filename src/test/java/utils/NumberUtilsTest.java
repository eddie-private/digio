package utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class NumberUtilsTest {

    @Test
    public void findTopEventSize_manyFirst() {
        assertEquals(3, NumberUtils.findTopXValue(Arrays.asList(9L, 9L, 9L, 9L, 4L, 3L, 1L), 3));
    }

    @Test
    public void findTopEventSize_manySecond() {
        assertEquals(4, NumberUtils.findTopXValue(Arrays.asList(9L, 7L, 7L, 7L, 4L, 3L, 1L), 3));
    }

    @Test
    public void findTopEventSize_manyThird() {
        assertEquals(7, NumberUtils.findTopXValue(Arrays.asList(9L, 8L, 7L, 7L, 7L, 7L, 1L), 3));
    }

    @Test
    public void findTopEventSize_lessThanTopNumber() {
        assertEquals(8L, NumberUtils.findTopXValue(Arrays.asList(9L, 8L), 3));
    }

    @Test
    public void findTopEventSize_emptyList() {
        assertThrows(IllegalArgumentException.class, () -> NumberUtils.findTopXValue(new ArrayList<>(), 3));
    }

    @Test
    public void findTopEventSize_null() {
        assertThrows(IllegalArgumentException.class, () -> NumberUtils.findTopXValue(null, 3));
    }
}

package log.parser;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import log.entity.LogEvent;

public class LogParserImplTest {

    private static final String LOG_FILE_PATH = "src/test/resources/programming-task-example-data.log";

    private String log1 = "177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] \"GET /intranet-analytics/ HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\"";
    private String logWithoutIP = "[10/Jul/2018:22:21:28 +0200] \"GET /intranet-analytics/ HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\"";

    private LogParserImpl logParser = new LogParserImpl();

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void parse_ipAddress() {
        LogEvent logEvent = logParser.parse(log1);

        assertEquals("177.71.128.21", logEvent.getIpAddress());
    }

    @Test
    public void parse_withoutIPAddress() {
        LogEvent logEvent = logParser.parse(logWithoutIP);

        assertEquals("", logEvent.getIpAddress());
    }

    @Test
    public void parseAll_IPAddress() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(LOG_FILE_PATH));

        while (scanner.hasNextLine()) {
            LogEvent logEvent = logParser.parse(scanner.nextLine());
            assertNotEquals("", logEvent.getIpAddress());
        }
    }

    @Test
    public void parse_Url() {
        assertEquals("/intranet-analytics/", logParser.parse(log1).getPath());
    }

    @Test
    public void parseAll_Url() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(LOG_FILE_PATH));

        while (scanner.hasNextLine()) {
            LogEvent logEvent = logParser.parse(scanner.nextLine());
            assertNotEquals("", logEvent.getPath());
        }
    }
}

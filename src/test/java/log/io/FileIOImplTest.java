package log.io;


import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class FileIOImplTest {

    private static final String FILE_PATH = "src/test/resources/programming-task-example-data.log";

    private FileIOImpl fileIO;

    @BeforeEach
    public void setUp() {
        fileIO = new FileIOImpl();
    }

    @Test
    public void read_filePathIsNull() {
        assertThrows(IllegalArgumentException.class, () -> fileIO.read(null));
    }

    @Test
    public void read_filePathIsEmpty() {
        assertThrows(FileNotFoundException.class, () -> fileIO.read(""));
    }

    @Test
    public void read_fileNotExist() {
        assertThrows(FileNotFoundException.class, () -> fileIO.read("not-exist-file-path"));
    }

    @Test
    public void read() throws IOException {
        File file = fileIO.read(FILE_PATH);

        assertNotNull(file);
    }
}

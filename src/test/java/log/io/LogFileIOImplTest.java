package log.io;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import log.entity.LogEvent;
import log.parser.LogParser;
import log.parser.LogParserImpl;

@ExtendWith(MockitoExtension.class)
public class LogFileIOImplTest {

    private static final String LOG_FILE_PATH = "src/test/resources/programming-task-example-data.log";
    private static final int EXPECTED_NUMBER_OF_LOG_EVENTS = 23;

    @Spy
    private FileIO fileIO = new FileIOImpl();

    @Spy
    private LogParser logParser = new LogParserImpl();

    @InjectMocks
    private LogFileIOImpl logFileIO = new LogFileIOImpl();

    @Test
    public void read_filePathIsNull() {
        assertThrows(IllegalArgumentException.class, () -> logFileIO.read(null));
    }

    @Test
    public void read_filePathIsEmpty() {
        assertThrows(FileNotFoundException.class, () -> logFileIO.read(""));
    }

    @Test
    public void read_fileNotExist() {
        assertThrows(FileNotFoundException.class, () -> logFileIO.read("not-exist-file-path"));
    }

    @Test
    public void read_correctLogEventNumber() throws IOException {
        List<LogEvent> logEvents = logFileIO.read(LOG_FILE_PATH);

        assertEquals(EXPECTED_NUMBER_OF_LOG_EVENTS, logEvents.size());
    }
}
